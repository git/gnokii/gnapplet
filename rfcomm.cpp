/*
 * $Id$
 *
 * G N A P P L E T
 *
 * gnapplet is a gnbus protocol driver for symbian phones.
 *
 * This file is part of gnokii.
 *
 * Gnokii is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gnokii is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with gnokii; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Copyright (C) 2004 BORBELY Zoltan
 *
 * This file provides RfCOMM support.
 *
 */

#include <btmanclient.h>

#include "rfcomm.h"

// gnapplet uuid is "0d755dee-9493-11dc-8314-0800200c9a66"
TUUID	gnapplet_uuid(0x0d755dee, 0x949311dc, 0x83140800, 0x200c9a66);


RfCOMM::~RfCOMM(void)
{
	CloseL();
	rSdpDb.DeleteRecordL(sdph);
	rSdpDb.Close();
	rSdp.Close();
	listen.Close();
	rserv.Close();
}


void RfCOMM::ConstructL(int port)
{
	TInt error;
	TBTSockAddr addr;
	RBTMan btSecManager;
	RBTSecuritySettings btSecSettings;
	TBTServiceSecurity btServiceSecurity(TUid::Uid(0x107d4df2), KSolBtRFCOMM, 0);
	TRequestStatus rs;

	// allocate an RfCOMM socket
	rserv.Connect();

	if ((error = listen.Open(rserv, _L("RFCOMM"))) != KErrNone) {
		User::Leave(error);
	}

	addr.SetPort(port);
	if ((error = listen.Bind(addr)) != KErrNone) {
		User::Leave(error);
	}

	listen.Listen(1);

	// register it into the security manager
	btSecManager.Connect();
	btSecSettings.Open(btSecManager);
	btServiceSecurity.SetAuthentication(TRUE);
	btServiceSecurity.SetEncryption(TRUE);
	btServiceSecurity.SetAuthorisation(TRUE);
	btServiceSecurity.SetChannelID(port);
	btSecSettings.RegisterService(btServiceSecurity, rs);
	User::WaitForRequest(rs);

	// add a record to the Service Discovery Protocol (SDP) Database
	User::LeaveIfError(rSdp.Connect());
	User::LeaveIfError(rSdpDb.Open(rSdp));
	rSdpDb.CreateServiceRecordL(gnapplet_uuid, sdph);

	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdServiceRecordHandle, sdph);

	CSdpAttrValueUUID *rootBrowseGroupAttr = CSdpAttrValueUUID::NewUUIDL(TUUID(0x1002));	// public browse group
	CleanupStack::PushL(rootBrowseGroupAttr);
	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdBrowseGroupList, *rootBrowseGroupAttr);
	CleanupStack::PopAndDestroy();

	CSdpAttrValueDES *serviceClassIDList = CSdpAttrValueDES::NewDESL(0);
	CleanupStack::PushL(serviceClassIDList);
	serviceClassIDList
	->StartListL()
		->BuildUUIDL(0x1101)
	->EndListL();
	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdServiceClassIDList, *serviceClassIDList);
	CleanupStack::PopAndDestroy();

	CSdpAttrValueDES *protocolDescriptorList = CSdpAttrValueDES::NewDESL(0);
	CleanupStack::PushL(protocolDescriptorList);
	TBuf8<1> rfcomm_port;
	rfcomm_port.Append(TUint8(port));
	protocolDescriptorList
	->StartListL()
		->BuildDESL()
		->StartListL()
			->BuildUUIDL(KL2CAP)
		->EndListL()
		->BuildDESL()
		->StartListL()
			->BuildUUIDL(KRFCOMM)
			->BuildUintL(rfcomm_port)
		->EndListL()
	->EndListL();
	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdProtocolDescriptorList, *protocolDescriptorList);
	CleanupStack::PopAndDestroy();

	CSdpAttrValueDES *languageBaseAttributeIDList = CSdpAttrValueDES::NewDESL(0);
	CleanupStack::PushL(languageBaseAttributeIDList);
	languageBaseAttributeIDList
	->StartListL()
		->BuildUintL(TPtrC8((TUint8 *)"\x65\x6e", 2))
		->BuildUintL(TPtrC8((TUint8 *)"\x00\x6a", 2))
		->BuildUintL(TPtrC8((TUint8 *)"\x01\x00", 2))
	->EndListL();
	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdLanguageBaseAttributeIDList, *languageBaseAttributeIDList);
	CleanupStack::PopAndDestroy();

	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdBasePrimaryLanguage + KSdpAttrIdOffsetServiceName, _L("gnapplet"));

	rSdpDb.UpdateAttributeL(sdph, KSdpAttrIdBasePrimaryLanguage + KSdpAttrIdOffsetServiceDescription, _L("gnokii driver to access symbian phones"));
}


void RfCOMM::AcceptL(TRequestStatus &rs)
{
	User::LeaveIfError(conn.Open(rserv));

	listen.Accept(conn, rs);
}


void RfCOMM::CloseL(void)
{
	conn.Close();
}


void RfCOMM::ReadL(TRequestStatus &rs, TDes8 &buf)
{
	conn.Read(buf, rs);
}


void RfCOMM::WriteL(TRequestStatus &rs, const TDes8 &buf)
{
	conn.Write(buf, rs);
}
